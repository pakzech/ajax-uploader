/*
 * Prototipo
 */
 // Constructor
function Uploader (settings) {
	this.accept = settings.accept;
	this.area = $(settings.area);
	this.classes = settings.classes;
	this.form = $(settings.form);
	this.icons = settings.icons;
	this.list = $(settings.list);
	this.max = settings.maxSize;
	this.messages = settings.messages;
	this.parseImage = settings.parseImage;
	this.parseText = settings.parseText;
	this.select = this.form.find('[type=file]');
}
// Método de agregado de archivos a la lista
Uploader.prototype.add = function (itemClass, content) {
	this.list
		.append($('<div />', { 'class': itemClass })
			.append(content));
};
// Manejador de eventos "dragover" y "dragleave"
Uploader.prototype.drag = function (event) {
	var _self_ = event.data.context;
	if (event.type === 'dragover') {
		console.log('Se está haciendo "dragover"');
		_self_.area.addClass(_self_.classes.drag);
	}
	else if (event.type === 'dragleave') {
		console.log('Se ha hecho "dragleave"');
		_self_.area.removeClass(_self_.classes.drag);
	}
	return false;
};
// Metodo de recepción de archivos por carga o arrastre
Uploader.prototype.drop = function (event) {
	console.log('Se ha hecho "drop"');
	var _self_ = event.data.context;
	var files = null;
	if (event.type === 'change') {
		files = event.target.files;
	}
	else if (event.type === 'drop') {
		files = event.originalEvent.dataTransfer.files;
	}
	for (var i = 0; i < files.length; i++) {
		_self_.read(files[i]);
	}
	_self_.area.removeClass(_self_.classes.drag);
	return false;
};
// Método de lectura y descarte de archivos
Uploader.prototype.read = function (file) {
	console.log(file);
	var _self_ = this;
	var raiseFileError = function (message) {
		var message = message.replace('[[file]]', file.name);
		var dismiss = $('<a />', { 'class': 'remove', href: '#' }).html('&times;');
		var content = $('<span />').text(message);
		console.log(message);
		_self_.add(_self_.classes.error, [content]);
	};
	var validElement = function (icon) {
		var bar = $('<span />', { 'class': 'bar' }).css({ width: 0 });
		var dismiss = $('<a />', { 'class': 'remove', href: '#' }).html('&times;');
		var icon = $('<span />', { 'class': 'icon' }).append(icon);
		var name = $('<span />', { 'class': 'name' }).text(file.name);
		var progress = $('<span />', { 'class': 'progress' }).append(bar);
		var size = $('<span />', { 'class': 'size' }).text(file.size + 'KB');
		_self_.add(_self_.classes.item, [progress, icon, name, size, dismiss]);
		_self_.upload(file, bar, _self_);
	};
	if (this.accept !== undefined && $.inArray(file.type, this.accept) === -1) {
		raiseFileError(this.messages.typeError);
	}
	else if (this.max !== undefined && file.size > this.max) {
		raiseFileError(this.messages.sizeError);
	}
	else {
		var reader = new FileReader();
		if (this.parseText && file.type.match(/text/)) {
			console.log('Se escaneará el archivo de texto...');
			reader.onload = function (event) {
				console.log(event);
				validElement(event.target.result);
			};
			reader.readAsText(file);
		}
		else if (this.parseImage !== false && file.type.match(/image/)) {
			console.log('Se escaneará la imagen...');
			reader.onload = function (event) {
				console.log(event);
				validElement($('<img />', { alt: file.type, src: event.target.result }));
			};
			reader.readAsDataURL(file);
		}
		else {
			console.log('El archivo será tratado como un binario');
			reader.onload = function (event) {
				console.log(event);
				var icon = null;
				if (_self_.icons.hasOwnProperty(file.type)) {
					icon = _self_.icons[file.type];
				}
				else {
					icon = _self_.icons.generic;
				}
				validElement($('<img />', { alt: file.type, src: icon }));
			};
			reader.readAsBinaryString(file);
		}
	}
};
// Método de carga de archivos
Uploader.prototype.upload = function (file, bar, context) {
	var _self_ = context;
	var xhr = new XMLHttpRequest();
	if (xhr.upload) {
		console.log('Se comenzará la carga del archivo...');
		var showProgress = function (event) {
			console.log(event);
			bar.width(parseInt(event.loaded / event.total * 100) + '%');
		};
		xhr.onreadystatechange = function (event) {
			if (xhr.readyState === 4) {
				if (xhr.status === 200) {
					bar.addClass(_self_.classes.success);
				}
				else {
					bar.addClass(_self_.classes.fail).width(100 + '%');
				}
			}
		};
		xhr.upload.addEventListener('progress', showProgress);
		xhr.open(this.form.attr('method'), this.form.attr('action'), true);
		xhr.setRequestHeader('X_FILENAME', file.name);
		xhr.send(file);
	}
	else {
		console.log('¡Ooooops! Este navegador no soporta la carga de archivos mediante XMLHttpRequest :(');
	}
};
// Método de inicialización de funcionalidades
Uploader.prototype.init = function () {
	if (window.File && window.FileList && window.FileReader) {
		console.log('Yeah! Este navegador soporta el API de archivos de HTML5 :D');
		this.form.find('[type=submit]').hide();
		this.area
			.on('dragover dragleave', { context: this }, this.drag)
			.on('drop', { context: this }, this.drop);
		this.select.on('change', { context: this }, this.drop);
	}
	else {
		console.log('¡Ooooops! Este navegador no soporta el API de archivos de HTML5 :(');
		this.area.hide();
	}
};

/*
 * Función de inicio
 */
var start = function () {
	// Bienvenida
	console.log('Hey, cruel, world...');
	// Creación de objeto
	var ajaxUploader = new Uploader({
		accept: ['image/png', 'image/jpeg', 'text/css', 'application/pdf', 'audio/mp3'],
		area: '#dragArea',
		classes: { drag: 'dragOver', error: 'fileError', fail: 'fail', item: 'listItem', success: 'success' },
		form: '#upload',
		icons: { 'application/pdf': 'img/icon-pdf.png', generic: 'img/icon-generic.gif' },
		list: '#files',
		maxSize: 500000000,
		messages: { typeError: '[[file]] no es un tipo de archivo permitido', sizeError: '[[file]] excede el tamaño máximo permitido por archivo' },
		parseImage: true,
		parseText: true
	});
	ajaxUploader.init();
};

/*
 * Código de inicio
 */
$(document).ready(start);